from flask import Flask, render_template, request
from nltk import PorterStemmer
from math import sqrt

import json
import time


local_path = "C:\\Users\\healf\\Documents\\College Work\\UC Irvine\\Spring 2020\\CS 121" \
             "\\ladoyle_CS121_hw3_SearchEngine\\"


# calculates the magnitude of a vector
def magnitude(vector):
    total = 0
    for val in vector:
        total += val ** 2
    return sqrt(total)


# calculates the dot product of two vectors of equal dimension
def dot_product(q, d):
    total = 0
    for i in range(len(q)):
        total += q[i] * d[i]
    return total


# Searcher class builds a web interface for searching inverted index
class Searcher(object):
    def __init__(self, url_map):
        self.app = Flask(__name__)
        self.url_id_map = url_map
        self.booking_index = dict()
        self.urls = dict()
        self.postings = dict()
        self.document_vector = dict()

        # flask method to render proper html document and display results
        @self.app.route('/', methods=['GET', 'POST'])
        def search_bar():
            if request.method == 'GET':
                return render_template('drake.html')
            else:
                start = int(round(time.time() * 1000))
                results = self.get_relevant_pages(request.form['search'])
                search_time = int(round(time.time() * 1000)) - start
                if not results:
                    return render_template('404.html', stime=search_time)
                return render_template(
                    'results.html', data=results, stime=search_time, query=request.form['search']
                )

    # takes the string of query terms and returns a mapping of pages and relevancy scores sorted by score
    def get_relevant_pages(self, query_string):
        self.clear_search()
        terms = query_string.split()
        ps = PorterStemmer()
        stems = sorted([ps.stem(term).lower() for term in terms])
        self.fill_postings(stems)
        if len(self.postings) < 1:
            return None
        relevancy_map = self.calculate_relevancy(stems)
        results = sorted(relevancy_map.items(), key=lambda x: x[1], reverse=True)
        if len(results) > 5:
            results = results[:5]
        return [self.url_id_map[url_id] for url_id, _ in results]

    # clears all storage previously used for postings, url-term maps, and document vector
    def clear_search(self):
        self.urls.clear()
        self.postings.clear()
        self.document_vector.clear()

    # calculates the relevancy of each document to the query using vectors and cosine similarity
    def calculate_relevancy(self, stems):
        query_vector = self.calculate_query_vector(stems)
        self.find_documents()
        self.calculate_document_vector(stems)
        relevancy_map = dict()
        for url in self.urls:
            # decided to skip authoritative scoring for now
            relevancy_map[url] = self.cosine_sim(query_vector, url)
        return relevancy_map

    # calculates the cosine similarity between two vectors of equal dimension
    def cosine_sim(self, q, d):
        doc_vector = self.document_vector[d]
        numerator = dot_product(q, doc_vector)
        denominator = magnitude(q) * magnitude(doc_vector)
        return numerator / denominator

    # builds a mapping of urls to terms/weights using dicts
    def find_documents(self):
        for key in self.postings:
            for post in self.postings[key]:
                url = post[0]
                if self.urls.get(url):
                    self.urls[url].update({key: post[1]})
                else:
                    self.urls[url] = {key: post[1]}
                    self.document_vector[url] = []

    # creates all document vectors using tf-idf weights
    def calculate_document_vector(self, stems):
        for url in self.urls:
            for stem in stems:
                if stem in self.urls[url]:
                    self.document_vector[url].append(self.urls[url][stem])
                else:
                    self.document_vector[url].append(0)

    # fills the query vector
    @staticmethod
    def calculate_query_vector(words):
        size = len(words)
        query_vector = [1] * size
        prev = None
        for i in range(size):
            word = words[i]
            if word == prev:
                query_vector[i] += 1
            prev = word
        return query_vector

    # reads the inverted index and loads all postings based on query terms
    def fill_postings(self, query_words):
        with open(local_path + 'rsrc\\Inverted Index.json', 'r') as index_file:
            for word in query_words:
                if self.booking_index.get(word):
                    index_file.seek(self.booking_index[word])
                    line = index_file.readline()
                    data = json.loads(line)
                    self.postings.update(data)

    # reads bookkeeping index into memory for O(1) search times
    def fill_booking_index(self):
        with open(local_path + 'rsrc\\bookkeeping_index.json', 'r') as book:
            temp_booking_index = json.loads(book.read())
            for key in temp_booking_index:
                self.booking_index[key] = int(temp_booking_index[key])

    # starts the flask app
    def run(self):
        self.fill_booking_index()
        self.app.run()
