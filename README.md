# Data Stream

Web search engine designed to implement classic and new information retrieval methods under strict conditions.
Corpus consists of pre-downloaded pages from University of California, Irvine, ICS network given by professor for class project.
All tokens are written to a file using an inverted index, and created with document duplicate detection (Simhash algorithm) and Porter stemming. Queries are resolved using a tf-idf relevancy score in under 300ms using a Flask built web UI.
