import json
import re

from bs4 import BeautifulSoup
from urllib.parse import urlparse
from nltk.stem import PorterStemmer
from hashlib import sha256


# creates a hash value for a url to help with storage lookup
def get_urlhash(url):
    parsed = urlparse(url)
    # everything other than scheme.
    return sha256(
        f"{parsed.netloc}/{parsed.path}/{parsed.params}/"
        f"{parsed.query}/{parsed.fragment}".encode("utf-8")).hexdigest()


# JSONReader class read json files and constructs inverted indices from the page content
class JSONReader(object):
    def __init__(self, inverted_index, marked_urls, indexer):
        self.keywords = set()
        self.i_index = inverted_index
        self.marked = marked_urls
        self.indexer = indexer

    # reads a json file and tokenizes the content
    def read_json(self, json_file):
        try:
            with open(json_file) as file:
                data = json.load(file)
            url = self.tokenize_page(data)
            self.keywords.clear()
            return url
        except IOError:
            return None

    # tokenizes web content and creates postings for dissimilar pages
    def tokenize_page(self, data):
        url = self.is_valid(data["url"])
        if not url:
            return None
        soup = BeautifulSoup(data["content"], 'html.parser')
        self.fill_keywords(soup)
        text = soup.get_text()
        page_words = []
        for word in re.split(r'\W+', text):
            if type(word) is str:
                word = word.encode('utf-8')
            if word.isalnum() and len(word) > 1:
                page_words.append(word.decode('utf-8').lower())
        freq_map = self.count_frequencies(page_words)
        if len(freq_map) < 25:
            return None
        fingerprint = self.indexer.create_fingerprint(freq_map)
        similarity = self.indexer.calculate_similarity(fingerprint)
        if similarity < 0.10:
            # near duplicate
            return None
        else:
            self.indexer.save_fingerprint(fingerprint)
        self.create_postings(freq_map, url)
        return url

    # saves a set of words as keywords based on important html tags
    def fill_keywords(self, soup):
        search_tags = ['title', 'b', 'h1', 'h2', 'h3', 'h4', 'h5', 'h6']
        for s_tag in search_tags:
            tags = soup.find_all(s_tag)
            for tag in tags:
                for word in re.split(r'\W+', str(tag.string)):
                    if type(word) is str:
                        word = word.encode('utf-8')
                    if word.isalnum() and len(word) > 1:
                        self.keywords.add(word.decode('utf-8').lower())

    # maps words to frequencies giving precedence to keywords
    def count_frequencies(self, words):
        freq_map = dict()
        ps = PorterStemmer()
        for word in words:
            if word in self.keywords:
                weight = 10
            else:
                weight = 1
            word = ps.stem(word)
            if freq_map.get(word):
                freq_map[word] += weight
            else:
                freq_map[word] = weight
        return freq_map

    # creates postings (list of id and tf) for every word in the frequency map
    def create_postings(self, freq_map, url):
        url_id = self.indexer.get_url_id(url)
        for word in freq_map:
            self.i_index[word] = [[url_id, freq_map[word]]]

    # url is not valid if seen before, doesn't have proper scheme, or has bad file extension
    def is_valid(self, url):
        parsed = urlparse(url)
        url = self.defrag(url)
        urlhash = get_urlhash(url)
        if urlhash in self.marked:
            return None
        elif parsed.scheme not in ('http', 'https'):
            return None
        elif re.match(
                r".*\.(css|js|bmp|gif|jpe?g|ico"
                + r"|png|tiff?|mid|mp2|mp3|mp4"
                + r"|wav|avi|mov|mpeg|ram|m4v|mkv|ogg|ogv|pdf"
                + r"|ps|eps|tex|ppt|pptx|ppsx|diff|doc|docx|xls|xlsx|names"
                + r"|data|dat|exe|bz2|tar|msi|bin|7z|psd|dmg|iso"
                + r"|epub|dll|cnf|tgz|sha1"
                + r"|thmx|mso|arff|rtf|jar|csv"
                + r"|rm|smil|wmv|swf|wma|zip|rar|gz)$", parsed.path.lower()):
            return None
        return url

    # strips url of fragments, and queries
    @staticmethod
    def defrag(url):
        parsed = urlparse(url)
        return f'{parsed.scheme}://{parsed.netloc}/{parsed.path}'
