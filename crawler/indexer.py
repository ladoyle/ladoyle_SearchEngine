import os
import json

from math import log
from string import ascii_lowercase
from hashlib import sha1
from threading import RLock

local_path = "C:\\Users\\healf\\Documents\\College Work\\UC Irvine\\Spring 2020\\CS 121" \
             "\\ladoyle_CS121_hw3_SearchEngine\\"


# Indexer class manages all file writes for creating the Inverted Index
class Indexer(object):
    def __init__(self):
        self.object_lock = RLock()
        self.write_lock = RLock()
        self.url_map_lock = RLock()
        self.fingerprints = list()
        self.inverted_index = dict()
        self.url_id_map = dict()
        self.booking_index = dict()
        self.file_id = 0
        self.num_pages = 0
        self.url_id = 0

    # saves num_pages and url/id map to files so that partial indices don't have to be rebuilt
    def save_progress(self):
        with open("rsrc\\Page Count.txt", "w") as count_file:
            count_file.write(str(self.num_pages))
        with open("rsrc\\ID Map.json", "w") as mapping_file:
            mapping_file.write(json.dumps(self.url_id_map))

    # retrieves the id/url mapping
    def get_url_map(self):
        if len(self.url_id_map) < 1:
            self.load_save_data()
        return self.url_id_map

    # sets a url in the id/url mapping and returns the id
    def get_url_id(self, url):
        self.url_map_lock.acquire()
        self.url_id_map[self.url_id] = url
        print(f'{self.url_id} -> {url}')
        self.url_id += 1
        self.url_map_lock.release()
        return self.url_id - 1

    # increases number of pages for calculating the idf score
    def increment_num_pages(self):
        self.object_lock.acquire()
        self.num_pages += 1
        self.object_lock.release()

    # stores the index from a worker into the main index
    def save_index(self, temp_index, worker_id):
        self.write_lock.acquire()
        for key in temp_index:
            if self.inverted_index.get(key):
                self.inverted_index[key] += temp_index[key]
            else:
                self.inverted_index[key] = temp_index[key]
        if len(self.inverted_index) > 100000:
            self.write_to_disk(worker_id)
        self.write_lock.release()

    # writes index to disk (json file) when in-memory index gets too big
    def write_to_disk(self, worker_id):
        print(f'\n**********  Worker: {worker_id} -> writing to disk  **********\n')
        with open(f'tmp\\temp_file_{self.file_id}.json', "w") as temp_file:
            keys = sorted(self.inverted_index)
            for key in keys:
                index_line = {key: self.inverted_index[key]}
                temp_file.write(json.dumps(index_line))
                temp_file.write("\n")
        self.inverted_index.clear()
        self.file_id += 1

    # creates one large index file from all partial indices
    def build_index(self):
        if len(self.inverted_index) > 0:
            self.write_to_disk(0)
        if self.num_pages == 0 or len(self.url_id_map) == 0:
            self.load_save_data()
        print('**********  Merging indices into one file  **********\n')
        files = [file for file in os.scandir(path=local_path + 'tmp\\')]
        self.iterate_files(files, '0')

        for ch in ascii_lowercase:
            self.iterate_files(files, ch)
        self.save_book_index()

    # writes the bookkeeping index to disk for O(1) search times
    def save_book_index(self):
        with open(local_path + 'rsrc\\bookkeeping_index.json', 'w') as book:
            book.write(json.dumps(self.booking_index))

    # loops through sections of the files based on flag parameter
    def iterate_files(self, files, flag):
        for file in files:
            with open(file) as temp_file:
                while True:
                    line = temp_file.readline()
                    if not line:
                        break
                    data = json.loads(line)
                    key = list(data.keys())[0]
                    table_id = key[0]
                    if flag.isdigit() and table_id.isalpha():
                        break
                    elif flag.isalpha() and (table_id.isdigit() or table_id < flag):
                        continue
                    elif flag.isalpha() and table_id > flag:
                        break
                    if self.inverted_index.get(key):
                        self.inverted_index[key] += data[key]
                    else:
                        self.inverted_index.update(data)
        self.convert_to_tfidf()
        self.write_index()

    # reads page count and url/id map into memory from files
    def load_save_data(self):
        with open(local_path + "rsrc\\Page Count.txt", "r") as count_file:
            self.num_pages = int(count_file.read())
        with open(local_path + "rsrc\\ID Map.json", "r") as mapping_file:
            temp_map = json.loads(mapping_file.read())
            for key in temp_map:
                self.url_id_map[int(key)] = temp_map[key]

    # converts tf scores in postings to tf-idf weights
    def convert_to_tfidf(self):
        for key in self.inverted_index:
            postings = self.inverted_index[key]
            for post in postings:
                post[1] *= 1 + log(self.num_pages / len(postings))

    # writes partial indices to disk when building full index
    def write_index(self):
        with open('rsrc\\Inverted Index.json', "a") as index:
            keys = sorted(self.inverted_index)
            for key in keys:
                self.booking_index[key] = index.tell()
                index_line = {key: self.inverted_index[key]}
                index.write(json.dumps(index_line))
                index.write("\n")
        self.inverted_index.clear()

    # creates a hash fingerprint for a page based on content using sha1
    @staticmethod
    def create_fingerprint(page_content):
        hex_scale = 16
        ini_hashes = []
        page_words = []
        vector_values = [0] * 160
        # hash all words
        for word in page_content:
            page_words.append(word)
            hash_hex = sha1(word.encode('utf-8')).hexdigest()
            hash_bin = bin(int(hash_hex, hex_scale))
            ini_hashes.append(hash_bin[2:])
        # add or subtract weights to value
        for i in range(len(ini_hashes)):
            hash_bin = ini_hashes[i]
            for j in range(len(hash_bin)):
                if hash_bin[j] == '0':
                    vector_values[j] -= page_content[page_words[i]]
                else:
                    vector_values[j] += page_content[page_words[i]]
        # build new fingerprint
        fingerprint = b''
        for value in vector_values:
            if value > 0:
                fingerprint += b'1'
            else:
                fingerprint += b'0'
        return fingerprint

    # calculates the similarity of two fingerprints based on hamming distance
    def calculate_similarity(self, new_hash):
        # calculate the distance of each page from the previous fingerprints
        distance = 1.0
        for fingerprint in self.fingerprints:
            distance = min(distance, self.hamming_distance(fingerprint, new_hash))
            if distance < 0.10:
                return distance
        return distance

    # calculates the hamming distances between two fingerprints based on bit differences
    @staticmethod
    def hamming_distance(fingerprint, new_hash):
        distance = 0
        size = len(fingerprint)
        for i in range(size):
            if fingerprint[i] != new_hash[i]:
                distance += 1
        return distance / size

    # stores a newly created fingerprint
    def save_fingerprint(self, fingerprint):
        self.write_lock.acquire()
        self.fingerprints.append(fingerprint)
        self.write_lock.release()
