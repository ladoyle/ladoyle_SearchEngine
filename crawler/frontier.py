import os
import re

from util import get_urlhash

from queue import Queue, Empty
from threading import Lock

local_path = "C:\\Users\\healf\\Documents\\College Work\\UC Irvine\\Spring 2020\\CS " \
             "121\\ladoyle_CS121_hw3_SearchEngine\\developer\\DEV"


# Frontier class stores all json files into a job Queue
class Frontier(object):
    def __init__(self):
        self.to_be_read = Queue()
        self.lock = Lock()
        self.marked_urls = dict()

    # function to fill the frontier only if indexing is needed
    def fill_frontier(self):
        print('**********  Reading json files into memory  *********\n')
        self.add_json(local_path)

    # adds all json file names to the job Queue
    def add_json(self, path):
        for entry in os.scandir(path=path):
            if os.path.isdir(entry):
                self.add_json(os.path.join(path, entry))
            elif re.match(r".+\.json$", entry.name):
                json_file = str(os.path.join(path, entry))
                self.to_be_read.put(json_file)

    # retrieves a job (json file) from the Queue
    def get_json_file(self):
        try:
            return self.to_be_read.get(block=True, timeout=3)
        except Empty:
            return None

    # marks visited urls so that we don't see duplicates
    def mark_file(self, url):
        self.lock.acquire()
        urlhash = get_urlhash(url)
        self.marked_urls[urlhash] = url
        self.lock.release()
