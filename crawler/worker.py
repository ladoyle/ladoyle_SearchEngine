from threading import Thread

from util import JSONReader


# Worker class is a thread wrapper that read files from the Frontier into the inverted index
class Worker(Thread):
    def __init__(self, worker_id, frontier, indexer):
        self.id_num = worker_id
        self.frontier = frontier
        self.indexer = indexer
        self.inverted_index = {}
        self.reader = JSONReader(self.inverted_index, self.frontier.marked_urls, self.indexer)
        super().__init__(daemon=True)

    # reads a file from the Frontier and creates an index for the inverted index
    def run(self):
        while True:
            file = self.frontier.get_json_file()
            if not file:
                break
            self.frontier.to_be_read.task_done()
            url = self.reader.read_json(file)
            if url:
                self.frontier.mark_file(url)
                self.indexer.increment_num_pages()
                self.indexer.save_index(self.inverted_index, self.id_num)
            self.inverted_index.clear()
