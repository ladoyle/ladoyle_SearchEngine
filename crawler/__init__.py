import os
import re

from crawler.worker import Worker
from crawler.frontier import Frontier
from crawler.indexer import Indexer

local_path = "C:\\Users\\healf\\Documents\\College Work\\UC Irvine\\Spring 2020\\CS " \
             "121\\ladoyle_CS121_hw3_SearchEngine\\"


# checks to see if the Inverted Index exists
def index_built():
    for entry in os.scandir(path=local_path + 'rsrc\\'):
        if re.match(r'Inverted Index\.json', entry.name):
            return True
    return False


# checks to see if partial indices have been created
def temp_files_exist():
    for entry in os.scandir(path=local_path + 'tmp\\'):
        if re.match(r'^temp.*', entry.name):
            return True
    return False


# Crawler class manages all threads for reading the corpus into an index
class Crawler(object):
    def __init__(self, frontier_factory=Frontier, worker_factory=Worker):
        self.workers = list()
        self.frontier = frontier_factory()
        self.worker_factory = worker_factory
        self.indexer = Indexer()

    # retrieves the id/url mapping from the indexer
    def get_url_map(self):
        return self.indexer.get_url_map()

    # creates and starts all threads
    def start_async(self):
        self.workers = [
            self.worker_factory(worker_id, self.frontier, self.indexer)
            for worker_id in range(50)
        ]
        print('**********  Starting page tokenization  **********\n')
        for reader in self.workers:
            reader.start()

    # starts the Crawler at the appropriate point
    def start(self):
        if index_built():
            print('**********  Index built: moving to search stage  **********\n')
            return
        if not temp_files_exist():
            self.frontier.fill_frontier()
            self.start_async()
            self.join()
            self.indexer.save_progress()
        self.indexer.build_index()

    # joins all the worker threads
    def join(self):
        for reader in self.workers:
            reader.join()