from crawler import Crawler
from searcher import Searcher


def main():
    crawler = Crawler()
    crawler.start()
    url_map = crawler.get_url_map()
    searcher = Searcher(url_map)
    searcher.run()


if __name__ == '__main__':
    main()
